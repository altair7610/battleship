package comz.spaceteam.data;

/**
 * Ship direction enum
 * @author vasko
 *
 */
public enum Direction {
	/**
	 * horizontal ship direction
	 */
	HORIZONTAL,
	/**
	 * vertical ship direction
	 */
	VERTICAL
}
